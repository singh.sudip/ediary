package np.com.ediary;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import np.com.ediary.ui.friend.FriendViewModel;

public class FriendActivity extends AppCompatActivity {

    private EditText etFirstName, etLastName, etAddress, etEmail, etPhone;
    private Spinner spinnerGender;
    private Button btnAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etAddress = findViewById(R.id.etAddress);
        spinnerGender = findViewById(R.id.spinnerGender);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        btnAction = findViewById(R.id.btnAction);

        boolean isCreateMode = true;
        FriendViewModel friend = new Gson().fromJson(getIntent().getStringExtra("friend"),
                FriendViewModel.class);
        if (friend != null) {
            isCreateMode = false;
            btnAction.setText("Update");
            etFirstName.setText(friend.getFirstName());
            etLastName.setText(friend.getLastName());
            etAddress.setText(friend.getAddress());
            spinnerGender.setSelection(friend.getGender());
            etEmail.setText(friend.getEmail());
            etPhone.setText(friend.getPhone());
        } else {
            btnAction.setText("Add");
        }
        String token = Utility.getToken(this);
        String headerBearerToken = "Bearer " + token;
        boolean finalIsCreateMode = isCreateMode;
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = etFirstName.getText().toString().trim();
                String lastName = etLastName.getText().toString().trim();
                String address = etAddress.getText().toString().trim();
                int gender = spinnerGender.getSelectedItemPosition();
                String email = etEmail.getText().toString().trim();
                String phone = etPhone.getText().toString().trim();
                if (!firstName.equals("") && !lastName.equals("") && !address.equals("")
                        && !email.equals("") && !phone.equals("")) {

                    String url = ApplicationConstants.API_BASE_URL_WITH_S + "friend/create/";
                    String method = "POST";
                    if (!finalIsCreateMode) {
                        method = "PUT";
                        url = ApplicationConstants.API_BASE_URL_WITH_S + "friend/" + friend.getId() + "/update/";
                    }
                    JsonObject json = new JsonObject();
                    json.addProperty("first_name", firstName);
                    json.addProperty("last_name", lastName);
                    json.addProperty("address", address);
                    json.addProperty("gender", gender);
                    json.addProperty("email", email);
                    json.addProperty("phone_no", phone);
                    Ion.with(getBaseContext())
                            .load(method, url)
                            .setHeader("Authorization", headerBearerToken)
                            .setJsonObjectBody(json)
                            .asJsonObject()
                            .withResponse()
                            .setCallback(new FutureCallback<Response<JsonObject>>() {
                                @Override
                                public void onCompleted(Exception e, Response<JsonObject> result) {
                                    if (e != null) {
                                        Toast.makeText(getBaseContext(), e.getMessage(),
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        int responseCode = result.getHeaders().code();
                                        Log.e("response", String.valueOf(result.getHeaders()
                                                .code()));
                                        if (responseCode != 200 && responseCode != 201) {
                                            Toast.makeText(getBaseContext(), R.string.error_message
                                                    , Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getBaseContext(), "Friend " +
                                                    ((responseCode == 200) ? "updated" : "created") +
                                                    " successfully.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                } else {
                    Toast.makeText(getBaseContext(), R.string.input_required, Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent main = new Intent(getBaseContext(), MainActivity.class);
        startActivity(main);
        finish();
        super.onBackPressed();
    }
}
