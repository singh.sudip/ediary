package np.com.ediary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin, btnSignUp;
    private EditText etUsername, etPassword;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btnLogin);
        btnSignUp = findViewById(R.id.btnSignUp);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);

        sharedpreferences = getSharedPreferences("token", Context.MODE_PRIVATE);
        /*String currentToken = sharedpreferences.getString("token", "");
        if (!currentToken.equals("")) {
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            intent.putExtra("token", currentToken);
            startActivity(intent);
        }*/


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                if (!username.trim().equals("") && !password.trim().equals("")) {
                    JsonObject json = new JsonObject();
                    json.addProperty("username", username);
                    json.addProperty("password", password);
                    Ion.with(getBaseContext())
                            .load(ApplicationConstants.API_BASE_URL + "token/")
                            .setJsonObjectBody(json)
                            .asJsonObject()
                            .withResponse()
                            .setCallback(new FutureCallback<Response<JsonObject>>() {
                                @Override
                                public void onCompleted(Exception e, Response<JsonObject> result) {
                                    if (e != null) {
                                        Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                    } else {
                                        int responseCode = result.getHeaders().code();
                                        Log.e("response", String.valueOf(result.getHeaders().code()));
                                        if (responseCode != 200) {
                                            Toast.makeText(getBaseContext(), result.getResult().get("detail").getAsString()
                                                    , Toast.LENGTH_SHORT).show();
                                        }else{
                                            String token = result.getResult().get("access").getAsString();
                                            SharedPreferences.Editor editor = sharedpreferences.edit();
                                            editor.putString("token", token);
                                            editor.commit();

                                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                            intent.putExtra("token", token);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            });
                } else {
                    Toast.makeText(getBaseContext(), R.string.login_required, Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(getBaseContext(), SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });
    }
}