package np.com.ediary;

import android.app.DatePickerDialog;
import android.os.Bundle;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import np.com.ediary.ui.meeting.MeetingViewModel;

public class MeetingActivity extends AppCompatActivity {

    private EditText etTitle, etVenue, etDateTime;
    private Spinner spinnerStatus;
    private Button btnDate, btnAction;
    private DatePickerDialog datePickerDialog;
    private Calendar newDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        etTitle = findViewById(R.id.etTitle);
        etVenue = findViewById(R.id.etVenue);
        etDateTime = findViewById(R.id.etDateTime);
        spinnerStatus = findViewById(R.id.spinnerStatus);
        btnDate = findViewById(R.id.btnDate);
        btnAction = findViewById(R.id.btnAction);

        newDate = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(
                this,
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view,
                                          int year, int monthOfYear,
                                          int dayOfMonth) {
                        newDate.set(year, monthOfYear, dayOfMonth);
                        etDateTime.setText(new DateTime(year, monthOfYear, dayOfMonth,
                                0, 0, 0).toString("yyyy-MM-dd"));
                    }
                }, newDate.get(Calendar.YEAR), newDate
                .get(Calendar.MONTH), newDate
                .get(Calendar.DAY_OF_MONTH));

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        boolean isCreateMode = true;
        Gson gson = Converters.registerDateTime(new GsonBuilder()).create();
        String meetingJson = getIntent().getStringExtra("meeting");
        MeetingViewModel meeting = gson.fromJson(meetingJson, MeetingViewModel.class);
        if (meeting != null) {
            isCreateMode = false;
            btnAction.setText("Update");
            etTitle.setText(meeting.getTitle());
            etVenue.setText(meeting.getVenue());
            DateTime meetingDate = meeting.getDate();
            etDateTime.setText(meetingDate.toString("yyyy-MM-dd"));
            datePickerDialog.updateDate(meetingDate.getYear(), meetingDate.getMonthOfYear(),
                    meetingDate.getDayOfMonth());
            int index = Arrays.asList(getResources().getStringArray(R.array.note_status_array))
                    .indexOf(meeting.getStatus());
            spinnerStatus.setSelection(index);
        } else {
            btnAction.setText("Add");
        }
        String token = Utility.getToken(this);
        String headerBearerToken = "Bearer " + token;
        boolean finalIsCreateMode = isCreateMode;
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = etTitle.getText().toString().trim();
                String venue = etVenue.getText().toString().trim();
                String status = spinnerStatus.getSelectedItem().toString();
                String date = etDateTime.getText().toString().trim();
                if (!title.equals("") && !venue.equals("") && !date.equals("")) {

                    String url = ApplicationConstants.API_BASE_URL_WITH_S + "meetings/create/";
                    String method = "POST";
                    if (!finalIsCreateMode) {
                        method = "PUT";
                        url = ApplicationConstants.API_BASE_URL_WITH_S + "meetings/" + meeting.getId() + "/update/";
                    }
                    JsonObject json = new JsonObject();
                    json.addProperty("title", title);
                    json.addProperty("venue", venue);
                    json.addProperty("status", status);
                    json.addProperty("date", date);
                    Ion.with(getBaseContext())
                            .load(method, url)
                            .setHeader("Authorization", headerBearerToken)
                            .setJsonObjectBody(json)
                            .asJsonObject()
                            .withResponse()
                            .setCallback(new FutureCallback<Response<JsonObject>>() {
                                @Override
                                public void onCompleted(Exception e, Response<JsonObject> result) {
                                    if (e != null) {
                                        Toast.makeText(getBaseContext(), e.getMessage(),
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        int responseCode = result.getHeaders().code();
                                        Log.e("response", String.valueOf(result.getHeaders()
                                                .code()));
                                        if (responseCode != 200 && responseCode != 201) {
                                            Toast.makeText(getBaseContext(), R.string.error_message
                                                    , Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getBaseContext(), "Meeting " +
                                                    ((responseCode == 200) ? "updated" : "created") +
                                                    " successfully.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                } else {
                    Toast.makeText(getBaseContext(), R.string.input_required, Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

}
