package np.com.ediary;

import android.content.Intent;
import android.os.Bundle;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Arrays;

import np.com.ediary.ui.note.NoteViewModel;

public class NoteActivity extends AppCompatActivity {

    private EditText etTitle, etDescription;
    private Spinner spinnerStatus;
    private Button btnAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        etTitle = findViewById(R.id.etTitle);
        etDescription = findViewById(R.id.etDescription);
        spinnerStatus = findViewById(R.id.spinnerStatus);
        btnAction = findViewById(R.id.btnAction);
        boolean isCreateMode = true;
        Gson gson = Converters.registerDateTime(new GsonBuilder()).create();
        String noteJson = getIntent().getStringExtra("note");
        NoteViewModel note = gson.fromJson(noteJson, NoteViewModel.class);
        if (note != null) {
            isCreateMode = false;
            btnAction.setText("Update");
            etTitle.setText(note.getTitle());
            etDescription.setText(note.getDescription());
            int index = Arrays.asList(getResources().getStringArray(R.array.note_status_array))
                    .indexOf(note.getStatus());
            spinnerStatus.setSelection(index);
        } else {
            btnAction.setText("Add");
        }
        String token = Utility.getToken(this);
        String headerBearerToken = "Bearer " + token;
        boolean finalIsCreateMode = isCreateMode;
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = etTitle.getText().toString().trim();
                String description = etDescription.getText().toString().trim();
                String status = spinnerStatus.getSelectedItem().toString();
                if (!title.equals("") && !description.equals("")) {

                    String url = ApplicationConstants.API_BASE_URL_WITH_S + "notes/create/";
                    String method = "POST";
                    if (!finalIsCreateMode) {
                        method = "PUT";
                        url = ApplicationConstants.API_BASE_URL_WITH_S + "notes/" + note.getId() + "/update/";
                    }
                    JsonObject json = new JsonObject();
                    json.addProperty("title", title);
                    json.addProperty("description", description);
                    json.addProperty("status", status);
                    Ion.with(getBaseContext())
                            .load(method, url)
                            .setHeader("Authorization", headerBearerToken)
                            .setJsonObjectBody(json)
                            .asJsonObject()
                            .withResponse()
                            .setCallback(new FutureCallback<Response<JsonObject>>() {
                                @Override
                                public void onCompleted(Exception e, Response<JsonObject> result) {
                                    if (e != null) {
                                        Toast.makeText(getBaseContext(), e.getMessage(),
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        int responseCode = result.getHeaders().code();
                                        Log.e("response", String.valueOf(result.getHeaders()
                                                .code()));
                                        if (responseCode != 200 && responseCode != 201) {
                                            Toast.makeText(getBaseContext(), R.string.error_message
                                                    , Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getBaseContext(), "Note " +
                                                    ((responseCode == 200) ? "updated" : "created") +
                                                    " successfully.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                } else {
                    Toast.makeText(getBaseContext(), R.string.input_required, Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent main = new Intent(getBaseContext(), MainActivity.class);
        startActivity(main);
        finish();
        super.onBackPressed();
    }

}
