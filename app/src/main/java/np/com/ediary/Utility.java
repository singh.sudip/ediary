package np.com.ediary;

import android.content.Context;
import android.content.SharedPreferences;

public class Utility {

    public static String getToken(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        String token = sharedpreferences.getString("token", "");
        return token;
    }
}
