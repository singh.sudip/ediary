package np.com.ediary.adapter.friend;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.List;

import np.com.ediary.ApplicationConstants;
import np.com.ediary.FriendActivity;
import np.com.ediary.MainActivity;
import np.com.ediary.R;
import np.com.ediary.Utility;
import np.com.ediary.ui.friend.FriendViewModel;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {
    private Context context;
    private List<FriendViewModel> friends;

    public FriendAdapter(Context context, List<FriendViewModel> friends) {
        this.context = context;
        this.friends = friends;
    }

    @Override
    public FriendAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false);
        return new FriendAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FriendAdapter.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        FriendViewModel friend = friends.get(position);
        holder.setFriendName(friend.getFullName());
        holder.setAddress(friend.getAddress());
        holder.setEmail(friend.getEmail());
        holder.setPhone(friend.getPhone());
        holder.setGender(friend.getGender());
        holder.setImage(friend.getCoverImage());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent friendIntent = new Intent(v.getContext(), FriendActivity.class);
                friendIntent.putExtra("friend", (new Gson()).toJson(friend));
                context.startActivity(friendIntent);
                Toast.makeText(holder.mView.getContext(), "Edit " + friend.getFullName(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bearerToken = "Bearer " + Utility.getToken(context);
                String url = ApplicationConstants.API_BASE_URL_WITH_S + "friend/" + friend.getId()
                        + "/delete/";
                Ion.with(context)
                        .load("DELETE", url)
                        .setHeader("Authorization", bearerToken)
                        .asJsonObject()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<JsonObject>>() {
                            @Override
                            public void onCompleted(Exception e, Response<JsonObject> result) {
                                if (e != null) {
                                    Toast.makeText(context, e.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    int responseCode = result.getHeaders().code();
                                    Log.e("response", String.valueOf(result.getHeaders()
                                            .code()));
                                    if (responseCode != 204) {
                                        Toast.makeText(context, R.string.error_message
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        friends.remove(friend);
                                        notifyDataSetChanged();
                                        Toast.makeText(context, "Friend deleted successfully.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
                Toast.makeText(holder.mView.getContext(), "Delete " + friend.getFullName(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (friends != null) {
            return friends.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvFriendName, tvAddress, tvEmail, tvPhone, tvGender;
        private ImageView ivFriend;
        private Button btnEdit, btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            btnEdit = mView.findViewById(R.id.btnEdit);
            btnDelete = mView.findViewById(R.id.btnDelete);
        }

        public void setFriendName(String friendName) {
            tvFriendName = mView.findViewById(R.id.tvFriendName);
            tvFriendName.setText(friendName);
        }

        public void setAddress(String address) {
            tvAddress = mView.findViewById(R.id.tvAddress);
            tvAddress.setText(address);
        }

        public void setEmail(String email) {
            tvEmail = mView.findViewById(R.id.tvEmail);
            tvEmail.setText(email);
        }

        public void setPhone(String phone) {
            tvPhone = mView.findViewById(R.id.tvPhone);
            tvPhone.setText(phone);
        }

        public void setGender(int gender) {
            tvGender = mView.findViewById(R.id.tvGender);
            String genderText = "";
            switch (gender) {
                case 1:
                    genderText = "Female";
                    break;
                case 2:
                    genderText = "Others";
                    break;
                default:
                    genderText = "Male";
                    break;
            }
            tvGender.setText(genderText);
        }

        public void setImage(String imageUrl) {
            ivFriend = mView.findViewById(R.id.ivFriend);
            Ion.with(ivFriend)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher_round)
                    .load(imageUrl);
        }
    }
}
