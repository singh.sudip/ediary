package np.com.ediary.adapter.meeting;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.joda.time.DateTime;

import java.util.List;

import np.com.ediary.ApplicationConstants;
import np.com.ediary.MainActivity;
import np.com.ediary.MeetingActivity;
import np.com.ediary.R;
import np.com.ediary.Utility;
import np.com.ediary.ui.meeting.MeetingViewModel;

public class MeetingAdapter extends RecyclerView.Adapter<MeetingAdapter.ViewHolder> {
    private Context context;
    private List<MeetingViewModel> meetings;

    public MeetingAdapter(Context context, List<MeetingViewModel> meetings) {
        this.context = context;
        this.meetings = meetings;
    }

    @Override
    public MeetingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_meeting, parent, false);
        return new MeetingAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MeetingAdapter.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        MeetingViewModel meeting = meetings.get(position);
        holder.setTitle(meeting.getTitle());
        holder.setDate(meeting.getDate());
        holder.setVenue(meeting.getVenue());
        holder.setStatus(meeting.getStatus());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Gson gson = Converters.registerDateTime(new GsonBuilder()).create();
                Intent meetingIntent = new Intent(v.getContext(), MeetingActivity.class);
                meetingIntent.putExtra("meeting", gson.toJson(meeting));
                context.startActivity(meetingIntent);
                Toast.makeText(holder.mView.getContext(), "Edit " + meeting.getTitle(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bearerToken = "Bearer " + Utility.getToken(context);
                String url = ApplicationConstants.API_BASE_URL_WITH_S + "meetings/" + meeting.getId()
                        + "/delete/";
                Ion.with(context)
                        .load("DELETE", url)
                        .setHeader("Authorization", bearerToken)
                        .asJsonObject()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<JsonObject>>() {
                            @Override
                            public void onCompleted(Exception e, Response<JsonObject> result) {
                                if (e != null) {
                                    Toast.makeText(context, e.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    int responseCode = result.getHeaders().code();
                                    Log.e("response", String.valueOf(result.getHeaders()
                                            .code()));
                                    if (responseCode != 204) {
                                        Toast.makeText(context, R.string.error_message
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        meetings.remove(meeting);
                                        notifyDataSetChanged();
                                        Toast.makeText(context, "Meeting deleted successfully.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
                Toast.makeText(holder.mView.getContext(), "Delete " + meeting.getTitle(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (meetings != null) {
            return meetings.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvTitle, tvDate, tvVenue, tvStatus;
        private Button btnEdit, btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            btnEdit = mView.findViewById(R.id.btnEdit);
            btnDelete = mView.findViewById(R.id.btnDelete);
        }

        public void setTitle(String title) {
            tvTitle = mView.findViewById(R.id.tvTitle);
            tvTitle.setText(title);
        }

        public void setDate(DateTime date) {
            tvDate = mView.findViewById(R.id.tvDate);
            tvDate.setText(date.toString("yyyy-MM-dd"));
        }

        public void setVenue(String venue) {
            tvVenue = mView.findViewById(R.id.tvVenue);
            tvVenue.setText(venue);
        }

        public void setStatus(String status) {
            tvStatus = mView.findViewById(R.id.tvStatus);
            tvStatus.setText(status);
        }
    }
}
