package np.com.ediary.adapter.note;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

import np.com.ediary.ApplicationConstants;
import np.com.ediary.MainActivity;
import np.com.ediary.NoteActivity;
import np.com.ediary.R;
import np.com.ediary.Utility;
import np.com.ediary.ui.note.NoteViewModel;


public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {
    private Context context;
    private List<NoteViewModel> notes;

    public NoteAdapter(Context context, List<NoteViewModel> notes) {
        this.context = context;
        this.notes = notes;
    }

    @Override
    public NoteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        return new NoteAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NoteAdapter.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        NoteViewModel note = notes.get(position);
        holder.setTitle(note.getTitle());
        holder.setCreatedDate(note.getCreatedDate());
        holder.setDescription(note.getDescription());
        holder.setStatus(note.getStatus());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Gson gson = Converters.registerDateTime(new GsonBuilder()).create();
                Intent noteIntent = new Intent(v.getContext(), NoteActivity.class);
                noteIntent.putExtra("note", gson.toJson(note));
                context.startActivity(noteIntent);
                Toast.makeText(holder.mView.getContext(), "Edit " + note.getTitle(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bearerToken = "Bearer " + Utility.getToken(context);
                String url = ApplicationConstants.API_BASE_URL_WITH_S + "notes/" + note.getId()
                        + "/delete/";
                Ion.with(context)
                        .load("DELETE", url)
                        .setHeader("Authorization", bearerToken)
                        .asJsonObject()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<JsonObject>>() {
                            @Override
                            public void onCompleted(Exception e, Response<JsonObject> result) {
                                if (e != null) {
                                    Toast.makeText(context, e.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    int responseCode = result.getHeaders().code();
                                    Log.e("response", String.valueOf(result.getHeaders()
                                            .code()));
                                    if (responseCode != 204) {
                                        Toast.makeText(context, R.string.error_message
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        notes.remove(note);
                                        notifyDataSetChanged();
                                        Toast.makeText(context, "Note deleted successfully.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
                Toast.makeText(holder.mView.getContext(), "Delete " + note.getTitle(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (notes != null) {
            return notes.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvTitle, tvCreatedDate, tvDescription, tvStatus;
        private Button btnEdit, btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            btnEdit = mView.findViewById(R.id.btnEdit);
            btnDelete = mView.findViewById(R.id.btnDelete);
        }

        public void setTitle(String title) {
            tvTitle = mView.findViewById(R.id.tvTitle);
            tvTitle.setText(title);
        }

        public void setCreatedDate(DateTime createdDate) {
            tvCreatedDate = mView.findViewById(R.id.tvCreatedDate);
            tvCreatedDate.setText(createdDate.toString("yyyy-MM-dd HH:mm:ss"));
        }

        public void setDescription(String description) {
            tvDescription = mView.findViewById(R.id.tvDescription);
            tvDescription.setText(description);
        }

        public void setStatus(String status) {
            tvStatus = mView.findViewById(R.id.tvStatus);
            tvStatus.setText(status);
        }
    }
}
