package np.com.ediary.ui.friend;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;

import np.com.ediary.ApplicationConstants;
import np.com.ediary.FriendActivity;
import np.com.ediary.R;
import np.com.ediary.adapter.friend.FriendAdapter;

public class FriendFragment extends Fragment {
    private ArrayList<FriendViewModel> friends;
    private FriendAdapter adapter;
    private RecyclerView rvFriends;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        friends = new ArrayList<>();
        String apiURL = ApplicationConstants.API_BASE_URL_WITH_S + "friend/";

        View root = inflater.inflate(R.layout.fragment_friend, container, false);
        rvFriends = root.findViewById(R.id.rvFriends);

        Button btnAddFriend = root.findViewById(R.id.btnAddFriend);
        btnAddFriend.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FriendActivity.class);
                startActivity(intent);
            }
        });

        Ion.with(getActivity())
                .load(apiURL)
                .asJsonArray()
                .withResponse()
                .setCallback(new FutureCallback<Response<JsonArray>>() {
                    @Override
                    public void onCompleted(Exception e, Response<JsonArray> result) {
                        if (e != null) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            int responseCode = result.getHeaders().code();
                            Log.e("response", String.valueOf(result.getHeaders().code()));
                            if (responseCode != 200) {
                                Toast.makeText(getActivity(), result.getResult().get(0)
                                                .getAsJsonObject().get("detail").getAsString()
                                        , Toast.LENGTH_SHORT).show();
                            } else {
                                JsonArray jsonArray = result.getResult();
                                for (int i = 0; i < jsonArray.size(); i++) {
                                    int id = 0, gender = 0;
                                    String firstName = "", lastName = "", address = "", email = "",
                                            phone = "", coverImage = "";
                                    JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
                                    if (jsonObj.has("pk"))
                                        id = jsonObj.get("pk").getAsInt();
                                    if (jsonObj.has("gender"))
                                        gender = jsonObj.get("gender").getAsInt();
                                    if (jsonObj.has("first_name"))
                                        firstName = jsonObj.get("first_name").getAsString();
                                    if (jsonObj.has("last_name"))
                                        lastName = jsonObj.get("last_name").getAsString();
                                    if (jsonObj.has("address"))
                                        address = jsonObj.get("address").getAsString();
                                    if (jsonObj.has("email"))
                                        email = jsonObj.get("email").getAsString();
                                    if (jsonObj.has("phone_no"))
                                        phone = jsonObj.get("phone_no").getAsString();
                                    if (jsonObj.has("cover_image"))
                                        coverImage = jsonObj.get("cover_image").getAsString();
                                    friends.add(new FriendViewModel(id, firstName, lastName, address,
                                            email, phone, gender, coverImage));
                                }
                                if (friends.size() > 0) {
                                    adapter = new FriendAdapter(getContext(), friends);
                                    rvFriends.setLayoutManager(new LinearLayoutManager(container.getContext()));
                                    rvFriends.setAdapter(adapter);
                                    rvFriends.setHasFixedSize(true);
                                } else
                                    Toast.makeText(getActivity(), "No Record Found.",
                                            Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });
        return root;
    }
}