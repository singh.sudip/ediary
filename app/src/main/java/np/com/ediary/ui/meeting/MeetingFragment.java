package np.com.ediary.ui.meeting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;

import np.com.ediary.ApplicationConstants;
import np.com.ediary.FriendActivity;
import np.com.ediary.MeetingActivity;
import np.com.ediary.R;
import np.com.ediary.adapter.meeting.MeetingAdapter;

public class MeetingFragment extends Fragment {
    private ArrayList<MeetingViewModel> meetings;
    private MeetingAdapter adapter;
    private RecyclerView rvMeetings;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        meetings = new ArrayList<>();
        String apiURL = ApplicationConstants.API_BASE_URL_WITH_S + "meetings/";

        View root = inflater.inflate(R.layout.fragment_meeting, container, false);
        rvMeetings = root.findViewById(R.id.rvMeetings);

        Button btnAddMeeting = root.findViewById(R.id.btnAddMeeting);
        btnAddMeeting.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MeetingActivity.class);
                startActivity(intent);
            }
        });

        Ion.with(getActivity())
                .load(apiURL)
                .asJsonArray()
                .withResponse()
                .setCallback(new FutureCallback<Response<JsonArray>>() {
                    @Override
                    public void onCompleted(Exception e, Response<JsonArray> result) {
                        if (e != null) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            int responseCode = result.getHeaders().code();
                            Log.e("response", String.valueOf(result.getHeaders().code()));
                            if (responseCode != 200) {
                                Toast.makeText(getActivity(), result.getResult().get(0)
                                                .getAsJsonObject().get("detail").getAsString()
                                        , Toast.LENGTH_SHORT).show();
                            } else {
                                JsonArray jsonArray = result.getResult();
                                for (int i = 0; i < jsonArray.size(); i++) {
                                    int id = 0;
                                    String title = "", venue = "", status = "";
                                    DateTime date = DateTime.now();
                                    JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
                                    if (jsonObj.has("pk"))
                                        id = jsonObj.get("pk").getAsInt();
                                    if (jsonObj.has("title"))
                                        title = jsonObj.get("title").getAsString();
                                    if (jsonObj.has("venue"))
                                        venue = jsonObj.get("venue").getAsString();
                                    if (jsonObj.has("status"))
                                        status = jsonObj.get("status").getAsString();
                                    if (jsonObj.has("date")) {
                                        date = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(
                                                jsonObj.get("date").getAsString());
                                    }
                                    meetings.add(new MeetingViewModel(id, title, venue, status, date));
                                }
                                if (meetings.size() > 0) {
                                    adapter = new MeetingAdapter(getContext(), meetings);
                                    rvMeetings.setLayoutManager(new LinearLayoutManager(container.getContext()));
                                    rvMeetings.setAdapter(adapter);
                                    rvMeetings.setHasFixedSize(true);
                                } else
                                    Toast.makeText(getActivity(), "No Record Found.",
                                            Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });
        return root;
    }
}