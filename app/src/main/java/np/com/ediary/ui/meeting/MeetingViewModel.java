package np.com.ediary.ui.meeting;

import org.joda.time.DateTime;

public class MeetingViewModel {
    private int id;
    private String title, venue, status;
    private DateTime date;

    public MeetingViewModel(int id, String  title, String venue, String status, DateTime date){
        super();
        this.setId(id);
        this.setTitle(title);
        this.setVenue(venue);
        this.setStatus(status);
        this.setDate(date);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
