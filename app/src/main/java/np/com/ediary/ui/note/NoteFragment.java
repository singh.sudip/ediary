package np.com.ediary.ui.note;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;

import np.com.ediary.ApplicationConstants;
import np.com.ediary.FriendActivity;
import np.com.ediary.NoteActivity;
import np.com.ediary.R;
import np.com.ediary.adapter.note.NoteAdapter;

public class NoteFragment extends Fragment {
    private ArrayList<NoteViewModel> notes;
    private NoteAdapter adapter;
    private RecyclerView rvNotes;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notes = new ArrayList<>();
        String apiURL = ApplicationConstants.API_BASE_URL_WITH_S + "notes/";

        View root = inflater.inflate(R.layout.fragment_note, container, false);
        rvNotes = root.findViewById(R.id.rvNotes);

        Button btnAddNote = root.findViewById(R.id.btnAddNote);
        btnAddNote.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NoteActivity.class);
                startActivity(intent);
            }
        });

        Ion.with(getActivity())
                .load(apiURL)
                .asJsonArray()
                .withResponse()
                .setCallback(new FutureCallback<Response<JsonArray>>() {
                    @Override
                    public void onCompleted(Exception e, Response<JsonArray> result) {
                        if (e != null) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            int responseCode = result.getHeaders().code();
                            Log.e("response", String.valueOf(result.getHeaders().code()));
                            if (responseCode != 200) {
                                Toast.makeText(getActivity(), result.getResult().get(0)
                                                .getAsJsonObject().get("detail").getAsString()
                                        , Toast.LENGTH_SHORT).show();
                            } else {
                                JsonArray jsonArray = result.getResult();
                                for (int i = 0; i < jsonArray.size(); i++) {
                                    int id = 0;
                                    String title = "", description = "", status = "";
                                    DateTime createdDate = DateTime.now();
                                    JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
                                    if (jsonObj.has("pk"))
                                        id = jsonObj.get("pk").getAsInt();
                                    if (jsonObj.has("title"))
                                        title = jsonObj.get("title").getAsString();
                                    if (jsonObj.has("description"))
                                        description = jsonObj.get("description").getAsString();
                                    if (jsonObj.has("status"))
                                        status = jsonObj.get("status").getAsString();
                                    if (jsonObj.has("created_date")) {
                                        createdDate = ISODateTimeFormat.dateTime().parseDateTime(jsonObj.get("created_date").getAsString());
                                    }
                                    notes.add(new NoteViewModel(id, title, description, status, createdDate));
                                }
                                if (notes.size() > 0) {
                                    adapter = new NoteAdapter(getContext(), notes);
                                    rvNotes.setLayoutManager(new LinearLayoutManager(container.getContext()));
                                    rvNotes.setAdapter(adapter);
                                    rvNotes.setHasFixedSize(true);
                                } else
                                    Toast.makeText(getActivity(), "No Record Found.",
                                            Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });
        return root;
    }
}