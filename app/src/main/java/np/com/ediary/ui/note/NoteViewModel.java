package np.com.ediary.ui.note;

import org.joda.time.DateTime;

public class NoteViewModel {
    private int id;
    private String title, description, status;
    private DateTime createdDate;

    public NoteViewModel(int id, String title, String description, String status, DateTime createdDate){
        super();
        this.setId(id);
        this.setTitle(title);
        this.setDescription(description);
        this.setStatus(status);
        this.setCreatedDate(createdDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }
}
